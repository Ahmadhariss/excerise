package aiia.app.developer.sample.viewholders;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import aiia.app.developer.sample.PhotoDetialActivity;
import aiia.app.developer.sample.R;
import aiia.app.developer.sample.objects.Photo;

/**
 * Created by Ahmad on 4/15/2017.
 */

public class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView imageView;
    private Photo photo;

    public PhotoViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);
    }

    @Override
    public void onClick(View view) {

        Intent photoDetailIntent = new Intent(view.getContext(), PhotoDetialActivity.class);
        photoDetailIntent.putExtra("item", this.photo);
        view.getContext().startActivity(photoDetailIntent);
    }

    public void bind(Photo photo) {
        this.photo = photo;
        Picasso.with(imageView.getContext())
                .load(this.photo.getThumbnailUrl().replace("http:", "https:"))
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .into(imageView);
    }
}
