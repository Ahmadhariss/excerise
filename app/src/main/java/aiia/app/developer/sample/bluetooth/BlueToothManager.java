package aiia.app.developer.sample.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.distance.DistanceCalculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.objects.BeaconItem;
import aiia.app.developer.sample.utils.Preferences;
import aiia.app.developer.sample.utils.Utils;

//import aiia.app.developer.sample.objects.Beacon;

/**
 * Created by Ahmad on 4/16/2017.
 */
@TargetApi(21)
public class BlueToothManager {
    private static BlueToothManager ourInstance;
    private static Context context;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 1000;
    private String TAG = "BTM";
    private String filterUUID = null;

    public boolean isStarted() {
        return Started;
    }

    private boolean Started = false;

    public void setFilterUUID(String filterUUID) {
        this.filterUUID = filterUUID;
    }

    private List<Beacon> beaconList = new ArrayList<>();
    DecimalFormat df = new DecimalFormat("0.000");
    private Runnable restartScan = new Runnable() {
        @Override
        public void run() {
            scanLeDevice();
        }
    };
    private OnBeaconScannedListener listener;


    public static BlueToothManager getInstance(Context context) {
        BlueToothManager.context = context;
        if (ourInstance == null) {
            ourInstance = new BlueToothManager(context);
        }
        return ourInstance;
    }

    public void setListener(OnBeaconScannedListener listener) {

        this.listener = listener;
    }

    private BlueToothManager(Context context) {

        final BluetoothManager bluetoothManager = (BluetoothManager) context
                .getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        mHandler = new Handler();
        Beacon.setDistanceCalculator(new DistanceCalculator() {
            @Override
            public double calculateDistance(int rssi, double txPower) {
                if (rssi == 0)
                    return -1.0D;
                double d1 = 1.0D * rssi / txPower;
                if (d1 < 1.0D) {

                    String d1val = df.format(Math.pow(d1, 10.0D));
                    if (isNumeric(d1val))
                        return Double.valueOf(d1val);// Math.pow(d1,// 10.0D);
                    else
                        return 0.023;

                }

                return Double.valueOf(df.format(0.111D + 0.89976D * Math.pow(d1,
                        7.7095D)));
            }
        });

    }

    public void startScanning() {
        if (bluetoothAdapter == null) {
            return;
        }

        Started = true;
        if (Build.VERSION.SDK_INT >= 21) {
            mLEScanner = bluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<ScanFilter>();
        }
        scanLeDevice();
    }

    private void scanLeDevice() {
        beaconList.clear();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT < 21) {
                    bluetoothAdapter.stopLeScan(mLeScanCallback);
                } else {
                    mLEScanner.stopScan(mScanCallback);
                }
                if (listener != null) {
                    listener.onScanned(beaconList);
                }
                mHandler.postDelayed(restartScan, Preferences.getBtRepeat(context));
            }
        }, SCAN_PERIOD);
        if (Build.VERSION.SDK_INT < 21) {
            bluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mLEScanner.startScan(filters, settings, mScanCallback);
        }

    }


    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice btDevice = result.getDevice();
            Beacon beacon = parseBeacon(btDevice, result.getRssi(), result.getScanRecord().getBytes());
            manageScannBeacontoList(beacon);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                BluetoothDevice btDevice = sr.getDevice();
                Beacon beacon = parseBeacon(btDevice, sr.getRssi(), sr.getScanRecord().getBytes());
                manageScannBeacontoList(beacon);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG + " Scan Failed", "Error Code: " + errorCode);
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi,
                                     final byte[] scanRecord) {
                    ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Beacon beacon = parseBeacon(device, rssi, scanRecord);
                            manageScannBeacontoList(beacon);
                        }
                    });
                }
            };


    public void stopScanning() {
        if (isStarted()) {
            mHandler.removeCallbacks(restartScan);
            if (Build.VERSION.SDK_INT < 21) {
                bluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback);
            }
        }
        Started = false;
    }

    private Beacon parseBeacon(BluetoothDevice device, int rssi,
                               byte[] scanRecord) {
        ArrayList<BeaconParser> beaconParsers = new ArrayList<BeaconParser>();
        //beaconParsers.add(new BeaconParser().setBeaconLayout(BeaconParser.URI_BEACON_LAYOUT));
        //beaconParsers.add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        //beaconParsers.add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_URL_LAYOUT));
        beaconParsers.add(new BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT));
        // IBeacon Layout
        beaconParsers.add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        Beacon beacon = null;
        for (BeaconParser parser : beaconParsers) {
            beacon = parser.fromScanData(scanRecord, rssi, device);
            if (beacon != null) {
                return beacon;
            }
        }
        return null;
    }

    private void manageScannBeacontoList(Beacon beacon) {
        if (beacon != null) {
            if (this.filterUUID == null || this.filterUUID.equalsIgnoreCase(beacon.getId1().toString())) {
                Log.i(TAG + " onLeScan LeScanCallback", beacon.toString());
                if (!beaconList.contains(beacon))
                    beaconList.add(beacon);
            }
        }
    }

    public interface OnBeaconScannedListener {
        void onScanned(List<Beacon> beaconList);
    }

    private static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void saveToCSV(List<BeaconItem> items) {
        StringBuilder sb = new StringBuilder();
        for (BeaconItem item : items) {
            sb.append(item.getUuid());
            sb.append(",");
            sb.append(item.getMajor());
            sb.append(",");
            sb.append(item.getMinor());
            sb.append(",");
            sb.append(item.getDistance());
            sb.append("\n");
        }
        // now we are appending the list generated to csv file.
        Utils.writeToCSVFile(Constants.BEACONS_CSV_FILE, sb.toString(), context);

    }
}
