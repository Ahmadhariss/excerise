package aiia.app.developer.sample.presenters;


import org.altbeacon.beacon.Beacon;

import java.util.ArrayList;
import java.util.List;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.api.APIUtils;
import aiia.app.developer.sample.objects.BeaconItem;
import aiia.app.developer.sample.objects.PostResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mad on 4/12/2017.
 */

public class PostBeaconsPresenter implements Callback<PostResponse> {

    public PostBeaconsPresenter() {

    }

    public void postBeacons(List<BeaconItem> beaconList) {

        APIUtils.getService(Constants.POSTS_BASE_URL).postBeacons(beaconList).enqueue(this);
    }


    @Override
    public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
        //success :)
    }

    @Override
    public void onFailure(Call<PostResponse> call, Throwable t) {
        //failed!
    }
}
