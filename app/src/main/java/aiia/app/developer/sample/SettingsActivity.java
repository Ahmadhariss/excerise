package aiia.app.developer.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import aiia.app.developer.sample.utils.Preferences;

public class SettingsActivity extends AppCompatActivity {
    private EditText gpsRepeat;
    private EditText bluetoothRepeat;
    private EditText wifiRepeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        gpsRepeat = (EditText) findViewById(R.id.gpsRepeat);
        bluetoothRepeat = (EditText) findViewById(R.id.bluetoothRepeat);
        wifiRepeat = (EditText) findViewById(R.id.wifiRepeat);

        gpsRepeat.setText(String.valueOf(Preferences.getGpsRepeat(this)));
        bluetoothRepeat.setText(String.valueOf(Preferences.getBtRepeat(this)));
        wifiRepeat.setText(String.valueOf(Preferences.getWifiRepeat(this)));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            Preferences.setGpsRepeat(this, Integer.parseInt(gpsRepeat.getText().toString()));
            Preferences.setBtRepeat(this, Integer.parseInt(bluetoothRepeat.getText().toString()));
            Preferences.setWifiRepeat(this, Integer.parseInt(wifiRepeat.getText().toString()));
            setResult(AppCompatActivity.RESULT_OK);
            finish();

        } else {
            cancelActivity();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        cancelActivity();
        //super.onBackPressed();
    }

    private void cancelActivity() {
        setResult(AppCompatActivity.RESULT_CANCELED);
        finish();
    }
}
