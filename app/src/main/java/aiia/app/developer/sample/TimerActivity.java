package aiia.app.developer.sample;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TimePicker;

import aiia.app.developer.sample.utils.AlarmHelper;
import aiia.app.developer.sample.utils.Preferences;

public class TimerActivity extends AppCompatActivity {
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private TimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        populateTimePicker();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                AlarmHelper.getInstance(this).setAlarm(timePicker.getHour(), timePicker.getMinute());
            } else {
                AlarmHelper.getInstance(this).setAlarm(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
            }
            setResult(AppCompatActivity.RESULT_OK);
            finish();

        } else {
            cancelActivity();
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        cancelActivity();
        //super.onBackPressed();
    }

    private void cancelActivity() {
        setResult(AppCompatActivity.RESULT_CANCELED);
        finish();
    }

    private void populateTimePicker() {
        int hour = Preferences.getAlarmHour(this);
        int min = Preferences.getAlarmMin(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (hour > -1) timePicker.setHour(hour);
            if (min > -1) timePicker.setMinute(min);
        } else {
            if (hour > -1) timePicker.setCurrentHour(hour);
            if (min > -1) timePicker.setCurrentMinute(min);
        }
    }

}
