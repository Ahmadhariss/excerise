package aiia.app.developer.sample.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import aiia.app.developer.sample.R;
import aiia.app.developer.sample.utils.AlarmHelper;
import aiia.app.developer.sample.utils.Preferences;
import aiia.app.developer.sample.utils.Utils;


/**
 * Created by Mad on 4/14/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    private static final int notificationId=2201;

    @Override
    public void onReceive(Context context, Intent intent) {
        Utils.showNotification(context,notificationId,context.getString(R.string.alarmNotification, Preferences.getAlarmHour(context),Preferences.getAlarmMin(context)),true);
        AlarmHelper.getInstance(context).cancel();
    }
}
