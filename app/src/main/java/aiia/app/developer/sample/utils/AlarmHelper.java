package aiia.app.developer.sample.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import aiia.app.developer.sample.receivers.AlarmReceiver;

/**
 * Created by Mad on 4/14/2017.
 */

public class AlarmHelper {
    private static AlarmHelper ourInstance;
    private AlarmManager alarmMgr;
    private Context context;

    public static AlarmHelper getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new AlarmHelper(context);
        }
        return ourInstance;
    }

    private AlarmHelper(Context context) {
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        this.context = context;
    }

    public void setAlarm(int hour, int min) {
        Intent intent = new Intent(this.context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this.context, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        alarmMgr.set(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), alarmIntent);
        Preferences.setAlarmHour(this.context, hour);
        Preferences.setAlarmMin(this.context, min);

    }

    public void cancel() {
        Intent intent = new Intent(this.context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this.context, 0, intent, 0);
        alarmMgr.cancel(alarmIntent);
        Preferences.clearAlarm(this.context);
    }
}
