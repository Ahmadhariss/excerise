package aiia.app.developer.sample.api;

import android.location.Location;
import android.net.wifi.ScanResult;

import java.util.List;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.objects.BeaconItem;
import aiia.app.developer.sample.objects.Photo;
import aiia.app.developer.sample.objects.PostResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ahmad on 4/12/2017.
 */

public interface APIService {
    @POST(Constants.POST_COORDINATE_URL)
    Call<PostResponse> postCoordinates(@Body Location loginRequest);

    @GET(Constants.GET_PHOTOS_URL)
    Call<List<Photo>> getPhotos();

    @POST(Constants.POST_BEACONS_URL)
    Call<PostResponse> postBeacons(@Body List<BeaconItem> beacons);

    @POST(Constants.POST_WIFI_URL)
    Call<PostResponse> postWifi(@Body List<ScanResult> beacons);
}
