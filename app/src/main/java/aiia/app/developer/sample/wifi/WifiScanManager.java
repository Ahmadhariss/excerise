package aiia.app.developer.sample.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.utils.Preferences;
import aiia.app.developer.sample.utils.Utils;

/**
 * Created by Ahmad on 4/16/2017.
 */

public class WifiScanManager {
    private static WifiScanManager instance;
    private Context context;
    private WifiManager wifi;

    public boolean isStarted() {
        return started;
    }

    private boolean started = false;
    private BroadcastReceiver wifiCallBack = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            List<ScanResult> results = wifi.getScanResults();
            Log.d("WIFIManager", results.toString());
            if (listener != null) {
                listener.onWifiScanned(results);
            }
        }
    };
    private OnWifiScannedListener listener;
    private Handler handler = new Handler();
    private Runnable restart = new Runnable() {
        @Override
        public void run() {
            scanWifi();
        }
    };

    private WifiScanManager(Context context) {
        this.context = context;
    }

    public static WifiScanManager getInstance(Context context) {
        if (instance == null) {
            instance = new WifiScanManager(context);
        }
        return instance;
    }

    public void setListener(OnWifiScannedListener listener) {
        this.listener = listener;
    }

    public void startScanning() {
        started = true;
        wifi = (WifiManager) this.context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifi.isWifiEnabled()) {
            Toast.makeText(this.context, "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }
        this.context.registerReceiver(wifiCallBack, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        scanWifi();
    }

    private void scanWifi() {
        wifi.startScan();
        handler.postDelayed(restart, Preferences.getWifiRepeat(context));
    }

    public void stopScanning() {
        if (started) {
            this.context.unregisterReceiver(wifiCallBack);
            handler.removeCallbacks(restart);
            started = false;
        }
    }

    public interface OnWifiScannedListener {
        void onWifiScanned(List<ScanResult> list);
    }

    public void saveToCSV(List<ScanResult> items) {
        StringBuilder sb = new StringBuilder();
        for (ScanResult item : items) {
            sb.append(item.BSSID);
            sb.append(",");
            sb.append(item.capabilities);
            sb.append(",");
            sb.append(item.level);
            sb.append(",");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sb.append(item.centerFreq0);
                sb.append(",");
                sb.append(item.centerFreq1);
                sb.append(",");
                sb.append(item.channelWidth);
                sb.append(",");
                sb.append(item.venueName);
                sb.append(",");
                sb.append(item.operatorFriendlyName);
                sb.append(",");
                sb.append(item.isPasspointNetwork());
                sb.append(",");
                sb.append(item.is80211mcResponder());
                sb.append(",");
            }
            sb.append(item.frequency);
            sb.append("\n");
        }
        // now we are appending the list generated to csv file.
        Utils.writeToCSVFile(Constants.WIFI_CSV_FILE, sb.toString(), context);

    }
}
