package aiia.app.developer.sample.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import aiia.app.developer.sample.R;
import aiia.app.developer.sample.objects.Photo;
import aiia.app.developer.sample.viewholders.PhotoViewHolder;

/**
 * Created by Mad on 4/15/2017.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

    private List<Photo> itemList = new ArrayList<>();
    private Context context;

    public PhotosAdapter(Context context) {
        this.context = context;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, null);
        PhotoViewHolder rcv = new PhotoViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bind(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public Photo getPhoto(int position) {
        return itemList.get(position);
    }

    public void setItemList(List<Photo> list) {
        this.itemList = list;
        notifyDataSetChanged();
    }
}