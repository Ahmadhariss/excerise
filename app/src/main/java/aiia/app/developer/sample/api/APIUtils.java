package aiia.app.developer.sample.api;

import aiia.app.developer.sample.Constants;

/**
 * Created by Mad on 4/12/2017.
 */

public class APIUtils {
    public static APIService getService(String baseurl) {
        return APIClient.getClient(baseurl).create(APIService.class);
    }
}
