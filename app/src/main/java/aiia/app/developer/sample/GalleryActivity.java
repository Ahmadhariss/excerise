package aiia.app.developer.sample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import aiia.app.developer.sample.adapters.PhotosAdapter;
import aiia.app.developer.sample.callbacks.PhotosCallback;
import aiia.app.developer.sample.objects.Photo;
import aiia.app.developer.sample.presenters.PhotosPresenter;

public class GalleryActivity extends AppCompatActivity implements PhotosCallback {
    private ProgressDialog dialog;
    private PhotosAdapter adapter;
    private GridLayoutManager lLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lLayout = new GridLayoutManager(this, 3);


        new PhotosPresenter(this).getPhotos();

        RecyclerView grid = (RecyclerView) findViewById(R.id.grid);
        grid.setHasFixedSize(true);
        grid.setLayoutManager(lLayout);
        adapter = new PhotosAdapter(this);

        grid.setAdapter(adapter);
    }


    @Override
    public void showLoader() {
        dialog = ProgressDialog.show(this, "",
                getString(R.string.loadingDailog), true);
    }

    @Override
    public void hideLoader() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void success(List<Photo> list) {
        adapter.setItemList(list);
    }

    @Override
    public void fail() {
        Toast.makeText(this, "Something Went wrong....", Toast.LENGTH_LONG).show();
    }
}
