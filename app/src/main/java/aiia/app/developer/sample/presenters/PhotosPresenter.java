package aiia.app.developer.sample.presenters;

import java.util.List;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.api.APIUtils;
import aiia.app.developer.sample.callbacks.PhotosCallback;
import aiia.app.developer.sample.objects.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mad on 4/15/2017.
 */

public class PhotosPresenter implements Callback<List<Photo>> {
    private PhotosCallback callback;

    public PhotosPresenter(PhotosCallback callback) {
        this.callback = callback;
    }

    public void getPhotos() {
        if (this.callback != null) {
            this.callback.showLoader();
        }
        APIUtils.getService(Constants.GET_PHOTOS_BASEURL).getPhotos().enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {

        if (this.callback != null) {
            if (response.body() != null) {
                this.callback.success(response.body());
            }
            this.callback.hideLoader();
        }
    }

    @Override
    public void onFailure(Call<List<Photo>> call, Throwable t) {
        if (this.callback != null) {
            this.callback.fail();
            this.callback.hideLoader();
        }
    }
}
