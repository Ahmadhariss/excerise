package aiia.app.developer.sample;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by Ahmad on 4/12/2017.
 */

public class Constants {
    public static final String LOCATION_CSV_FILE = "locations.csv";
    public static final String CALLS_CSV_FILE = "callLog.csv";
    public static final String BEACONS_CSV_FILE = "beacons.csv";
    public static final String WIFI_CSV_FILE = "wifi.csv";
    public static final String POSTS_BASE_URL = "https://any.base.url.here";
    public static final String POST_COORDINATE_URL = "/post/coordiate";
    public static final String POST_BEACONS_URL = "/post/beacons";
    public static final String POST_WIFI_URL = "/post/wifi";

    public static final String GET_PHOTOS_BASEURL = "https://jsonplaceholder.typicode.com";
    public static final String GET_PHOTOS_URL = "/photos";

    public static final String UUID = "8fb95e76-18dc-4c1f-809d-2f2553a74ac9";

    public static final HashMap<String, LatLng> GEO_FENCES = new HashMap<String, LatLng>();

    static {
        GEO_FENCES.put("HAMRA", new LatLng(33.8965998, 35.4122601));
        GEO_FENCES.put("PARC", new LatLng(33.8731721, 35.5131544));
        GEO_FENCES.put("Military Hospital", new LatLng(33.874245, 35.512874));
        GEO_FENCES.put("Home", new LatLng(33.664435, 35.449146));
    }

    public static final float GEOFENCE_RADIUS_IN_METERS = 100;

    public static int DEFAULT_GPS_REPEAT = 10000;
    public static int DEFAULT_BT_REPEAT = 30000;
    public static int DEFAULT_WIFI_REPEAT = 60000;
}
