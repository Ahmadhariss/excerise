package aiia.app.developer.sample.utils;

/**
 * Created by Mad on 4/13/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import aiia.app.developer.sample.Constants;


public class Preferences {

    private static final String GEOFENCEADDED = "GEOFENCEADDED";
    private static final String ALARM_HOUR = "ALARM_HOUR";
    private static final String ALARM_MIN = "ALARM_MIN";
    private static final String GPS_REPEAT = "GPS_REPEAT";
    private static final String BT_REPEAT = "BT_REPEAT";
    private static final String WIFI_REPEAT = "WIFI_REPEAT";


    public static SharedPreferences getSharedPreferences(Context c) {
        return c.getSharedPreferences("AIIAPREF", Context.MODE_PRIVATE);
    }

    public static Editor getEditor(Context c) {
        return getSharedPreferences(c).edit();
    }

    private Preferences() {
    }

    public static void setGeoFence(Context context, boolean isAdded) {
        getEditor(context).putBoolean(GEOFENCEADDED, isAdded).apply();
    }

    public static boolean isGeoFenceAdded(Context context) {
        return getSharedPreferences(context).getBoolean(GEOFENCEADDED, false);
    }

    public static void setAlarmHour(Context context, int alarmHour) {
        getEditor(context).putInt(ALARM_HOUR, alarmHour).apply();
    }

    public static void setAlarmMin(Context context, int alarmMin) {
        getEditor(context).putInt(ALARM_MIN, alarmMin).apply();
    }

    public static int getAlarmHour(Context context) {
        return getSharedPreferences(context).getInt(ALARM_HOUR, -1);
    }

    public static int getAlarmMin(Context context) {
        return getSharedPreferences(context).getInt(ALARM_MIN, -1);
    }

    public static void clearAlarm(Context context) {
        getEditor(context).remove(ALARM_HOUR).apply();
        getEditor(context).remove(ALARM_MIN).apply();
    }

    public static void setGpsRepeat(Context context, int gpsRepeat) {
        getEditor(context).putInt(GPS_REPEAT, gpsRepeat).apply();
    }

    public static void setBtRepeat(Context context, int btRepeat) {
        getEditor(context).putInt(BT_REPEAT, btRepeat).apply();
    }

    public static void setWifiRepeat(Context context, int wifiRepeat) {
        getEditor(context).putInt(WIFI_REPEAT, wifiRepeat).apply();
    }

    public static int getGpsRepeat(Context context) {
        return getSharedPreferences(context).getInt(GPS_REPEAT, Constants.DEFAULT_GPS_REPEAT);
    }

    public static int getBtRepeat(Context context) {
        return getSharedPreferences(context).getInt(GPS_REPEAT, Constants.DEFAULT_BT_REPEAT);
    }

    public static int getWifiRepeat(Context context) {
        return getSharedPreferences(context).getInt(GPS_REPEAT, Constants.DEFAULT_WIFI_REPEAT);
    }
}