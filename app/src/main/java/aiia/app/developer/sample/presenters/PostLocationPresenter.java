package aiia.app.developer.sample.presenters;


import android.location.Location;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.api.APIUtils;
import aiia.app.developer.sample.objects.PostResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mad on 4/12/2017.
 */

public class PostLocationPresenter implements Callback<PostResponse> {

    public PostLocationPresenter() {

    }

    public void postLocation(Location location) {
        APIUtils.getService(Constants.POSTS_BASE_URL).postCoordinates(location).enqueue(this);
    }


    @Override
    public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
        //success :)
    }

    @Override
    public void onFailure(Call<PostResponse> call, Throwable t) {
        //failed!
    }
}
