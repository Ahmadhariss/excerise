package aiia.app.developer.sample.callbacks;

import java.util.List;

import aiia.app.developer.sample.objects.Photo;

/**
 * Created by Mad on 4/15/2017.
 */

public interface PhotosCallback {
    void showLoader();
    void hideLoader();
    void success(List<Photo> list);
    void fail();
}
