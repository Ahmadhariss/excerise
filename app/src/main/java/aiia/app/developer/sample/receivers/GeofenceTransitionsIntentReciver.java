package aiia.app.developer.sample.receivers;

/**
 * Created by Mad on 4/13/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import aiia.app.developer.sample.utils.Utils;


/**
 * Created by User on 1/4/2016.
 */
public class GeofenceTransitionsIntentReciver extends BroadcastReceiver {
    Context context;
    private static final String TAG = "GeofenceReciver";
    private static final int notificationID = 10023;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if (geofencingEvent.hasError()) {
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();


        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            for (Geofence geofence : geofencingEvent.getTriggeringGeofences()) {
                Utils.showNotification(context, notificationID, "Entered Region " + geofence.getRequestId(), true);
            }


        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            for (Geofence geofence : geofencingEvent.getTriggeringGeofences()) {
                Utils.showNotification(context, notificationID, "Entered Region " + geofence.getRequestId(), true);
            }

        } else {
            Log.d(TAG, "unknown");
        }


    }


}