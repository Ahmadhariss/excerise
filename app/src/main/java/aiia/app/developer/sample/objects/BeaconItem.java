package aiia.app.developer.sample.objects;

import org.altbeacon.beacon.Beacon;

/**
 * Created by Mad on 4/16/2017.
 */

public class BeaconItem {
    private String uuid;
    private String major;
    private String minor;
    private int rssi;
    private double distance;

    public BeaconItem() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BeaconItem that = (BeaconItem) o;

        if (rssi != that.rssi) return false;
        if (Double.compare(that.distance, distance) != 0) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (major != null ? !major.equals(that.major) : that.major != null) return false;
        return minor != null ? minor.equals(that.minor) : that.minor == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (major != null ? major.hashCode() : 0);
        result = 31 * result + (minor != null ? minor.hashCode() : 0);
        result = 31 * result + rssi;
        temp = Double.doubleToLongBits(distance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "BeaconItem{" +
                "uuid='" + uuid + '\'' +
                ", major='" + major + '\'' +
                ", minor='" + minor + '\'' +
                ", rssi=" + rssi +
                ", distance=" + distance +
                '}';
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public BeaconItem(Beacon beacon) {
        this.uuid = beacon.getId1().toUuidString();
        this.major = beacon.getId2().toString();
        this.minor = beacon.getId3().toString();
        this.rssi = beacon.getRssi();
        this.distance = beacon.getDistance();



    }

    public String getUuid() {

        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }


    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

}
