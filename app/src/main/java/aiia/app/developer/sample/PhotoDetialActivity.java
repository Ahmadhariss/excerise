package aiia.app.developer.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import aiia.app.developer.sample.objects.Photo;

public class PhotoDetialActivity extends AppCompatActivity {
    private ImageView mContentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photo_detial);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mContentView = (ImageView) findViewById(R.id.fullscreen_content);
        TextView titleView = (TextView) findViewById(R.id.titleView);
        Photo photo = getIntent().getParcelableExtra("item");
        if (photo != null) {
            Picasso.with(this).load(photo.getUrl().replace("http:", "https:")).fit().into(mContentView);
            titleView.setText(photo.getTitle());
            setTitle(photo.getTitle());
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}
