package aiia.app.developer.sample.services;

import android.Manifest;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Map;

import aiia.app.developer.sample.Constants;
import aiia.app.developer.sample.receivers.GeofenceTransitionsIntentReciver;
import aiia.app.developer.sample.utils.Preferences;

import static com.google.android.gms.location.Geofence.NEVER_EXPIRE;


/**
 * Created by User on 9/30/2015.
 */
public class ManageGeofences extends IntentService implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private PendingIntent pi;
    private ArrayList<Geofence> mGeofenceList;
    private GoogleApiClient mGoogleApiClient;
    private static final int ADD_MODE = 1;
    private static final int REMOVE_MODE = 2;
    private static int MODE = 0;
    public static String GEOFENCE_ADD_ACTION = "add_geofence";
    public static String GEOFENCE_REMOVE_ACTION = "remove_geofence";


    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (MODE == ADD_MODE) {
            try {
                if (mGoogleApiClient.isConnected()) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    Log.d(TAG, "geofence added");
                    LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, getGeofencingRequest(), pi);
                    Preferences.setGeoFence(ManageGeofences.this, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (MODE == REMOVE_MODE) {
            try {
                // Remove geofences.
                LocationServices.GeofencingApi.removeGeofences(
                        mGoogleApiClient,
                        // This is the same pending intent that was used in addGeofences().
                        pi
                );
                Log.d(TAG, "geofence removed");
                Preferences.setGeoFence(ManageGeofences.this, false);
            } catch (SecurityException securityException) {
                securityException.printStackTrace();
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected static final String TAG = "GeoFenceSetService";


    public ManageGeofences() {
        super(TAG);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        initGeofence(getApplicationContext());
        if (intent.getAction() == GEOFENCE_ADD_ACTION) {
            MODE = ADD_MODE;
        } else if (intent.getAction() == GEOFENCE_REMOVE_ACTION) {
            MODE = REMOVE_MODE;
        }
    }

    /*this method is to initialize the geofence*/
    private void initGeofence(Context context) {
        mGeofenceList = new ArrayList<>();

        Intent intent = new Intent(context, GeofenceTransitionsIntentReciver.class);
        pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        populateGeofenceList();
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }


    public void populateGeofenceList() {
        for (Map.Entry<String, LatLng> entry : Constants.GEO_FENCES.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(entry.getKey())

                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )
                    .setExpirationDuration(NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());
        }
    }

}