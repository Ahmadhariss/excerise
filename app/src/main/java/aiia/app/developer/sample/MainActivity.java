package aiia.app.developer.sample;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import org.altbeacon.beacon.Beacon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import aiia.app.developer.sample.bluetooth.BlueToothManager;
import aiia.app.developer.sample.objects.BeaconItem;
import aiia.app.developer.sample.presenters.PostBeaconsPresenter;
import aiia.app.developer.sample.presenters.PostWifiPresenter;
import aiia.app.developer.sample.receivers.PhoneCallReceiver;
import aiia.app.developer.sample.services.LocationService;
import aiia.app.developer.sample.services.ManageGeofences;
import aiia.app.developer.sample.utils.AlarmHelper;
import aiia.app.developer.sample.utils.Preferences;
import aiia.app.developer.sample.utils.Utils;
import aiia.app.developer.sample.wifi.WifiScanManager;

public class MainActivity extends PermissionActivity implements CompoundButton.OnCheckedChangeListener, BlueToothManager.OnBeaconScannedListener, WifiScanManager.OnWifiScannedListener {

    private static final int TIMERCODE = 1;
    private static final int SETTINGSCODE = 2;
    private SwitchCompat alarmButton;
    private SwitchCompat callLogButton;
    private PhoneCallReceiver phoneCallReceiver;
    private SwitchCompat BLEButton;
    private SwitchCompat BLESpecificButton;
    private SwitchCompat geoFenceButton;
    private SwitchCompat locationButton;
    List<String> permissions = new ArrayList<>(Arrays.asList(LOCATION_PERMISSION,
            READ_CONTACTS,
            COARSE_LOCATION,
            READ_CALL_LOG,
            READ_EXTERNAL_STORAGE,
            WRITE_EXTERNAL_STORAGE,
            READ_PHONE_STATE,
            PROCESS_OUTGOING_CALL
    ));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        locationButton = (SwitchCompat) findViewById(R.id.locationButton);
        geoFenceButton = (SwitchCompat) findViewById(R.id.geoFenceButton);
        alarmButton = (SwitchCompat) findViewById(R.id.alarmButton);
        callLogButton = (SwitchCompat) findViewById(R.id.callLogButton);
        BLEButton = (SwitchCompat) findViewById(R.id.BLEButton);
        BLESpecificButton = (SwitchCompat) findViewById(R.id.BLESpecificButton);
        SwitchCompat WifiButton = (SwitchCompat) findViewById(R.id.WifiButton);
        locationButton.setChecked(Utils.isMyServiceRunning(this, LocationService.class));
        geoFenceButton.setChecked(Preferences.isGeoFenceAdded(this));
        alarmButton.setChecked(Preferences.getAlarmHour(this) != -1);

        findViewById(R.id.galllery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntnet = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(galleryIntnet);
            }
        });


        locationButton.setOnCheckedChangeListener(this);
        geoFenceButton.setOnCheckedChangeListener(this);
        alarmButton.setOnCheckedChangeListener(this);
        callLogButton.setOnCheckedChangeListener(this);
        BLEButton.setOnCheckedChangeListener(this);
        BLESpecificButton.setOnCheckedChangeListener(this);
        WifiButton.setOnCheckedChangeListener(this);


        checkPermission(permissions);


        // requesting for permissions:


    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.locationButton:
                Log.d("LocationService", "" + isChecked);
                if (isChecked) {
                    startService(new Intent(this, LocationService.class));
                } else {
                    stopService(new Intent(this, LocationService.class));
                }
                break;
            case R.id.geoFenceButton:
                Intent intent = new Intent(MainActivity.this, ManageGeofences.class);
                if (isChecked) {
                    intent.setAction(ManageGeofences.GEOFENCE_ADD_ACTION);
                } else {
                    intent.setAction(ManageGeofences.GEOFENCE_REMOVE_ACTION);
                }
                startService(intent);

                break;
            case R.id.alarmButton:
                if (isChecked) {
                    Intent alarmIntent = new Intent(this, TimerActivity.class);
                    startActivityForResult(alarmIntent, TIMERCODE);
                } else {
                    AlarmHelper.getInstance(this).cancel();
                }
                break;
            case R.id.callLogButton:
                if (isChecked) {
                    Log.d("MainActivity", "checked!");
                    registerPhoneReciver();
                } else {
                    Log.d("MainActivity", "un checked!");
                    unregisterPhoneReiver();
                }
                break;
            case R.id.BLEButton:
                if (isChecked) {
                    BLESpecificButton.setChecked(false);
                    BlueToothManager.getInstance(this).setFilterUUID(null);
                    if (!BlueToothManager.getInstance(this).isStarted()) {
                        BlueToothManager.getInstance(this).startScanning();
                    }
                } else {
                    if (!BLESpecificButton.isChecked()) {
                        BlueToothManager.getInstance(this).stopScanning();
                    }
                }
                break;
            case R.id.BLESpecificButton:
                if (isChecked) {
                    BLEButton.setChecked(false);
                    BlueToothManager.getInstance(this).setFilterUUID(Constants.UUID);
                    if (!BlueToothManager.getInstance(this).isStarted()) {
                        BlueToothManager.getInstance(this).startScanning();
                    }
                } else {
                    if (!BLEButton.isChecked()) {
                        BlueToothManager.getInstance(this).setFilterUUID(null);
                        BlueToothManager.getInstance(this).stopScanning();
                    }
                }
            case R.id.WifiButton:
                if (isChecked) {
                    WifiScanManager.getInstance(this).startScanning();
                } else {
                    WifiScanManager.getInstance(this).stopScanning();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            if (requestCode == TIMERCODE) {
                alarmButton.setChecked(false);
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == SETTINGSCODE) {
                if (Utils.isMyServiceRunning(this, LocationService.class)) {
                    stopService(new Intent(this, LocationService.class));
                    startService(new Intent(this, LocationService.class));
                }
                if (BlueToothManager.getInstance(this).isStarted()) {
                    BlueToothManager.getInstance(this).stopScanning();
                    BlueToothManager.getInstance(this).startScanning();
                }
                if (WifiScanManager.getInstance(this).isStarted()) {
                    WifiScanManager.getInstance(this).stopScanning();
                    WifiScanManager.getInstance(this).startScanning();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void registerPhoneReciver() {
        phoneCallReceiver = new PhoneCallReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
        registerReceiver(phoneCallReceiver, filter);
    }

    private void unregisterPhoneReiver() {
        if (phoneCallReceiver != null)
            try {
                unregisterReceiver(phoneCallReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterPhoneReiver();
        BlueToothManager.getInstance(this).stopScanning();
        WifiScanManager.getInstance(this).stopScanning();

    }

    @Override
    public void onScanned(List<Beacon> beaconList) {
        //we need to post beacons here to a server.
        if (beaconList.size() > 0) {
            List<BeaconItem> list = new ArrayList<>();
            for (Beacon beacon : beaconList) {
                list.add(new BeaconItem(beacon));
            }
            PostBeaconsPresenter presenter = new PostBeaconsPresenter();
            presenter.postBeacons(list);
            BlueToothManager.getInstance(this).saveToCSV(list);
        }
    }

    @Override
    public void onWifiScanned(List<ScanResult> list) {
        PostWifiPresenter presenter = new PostWifiPresenter();
        presenter.postWifi(list);
        WifiScanManager.getInstance(this).saveToCSV(list);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), SETTINGSCODE);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void permissionGranted() {
        super.permissionGranted();
        //creating singleton instance of bluetooth manager and setting call back for it.
        BlueToothManager.getInstance(this).setListener(this);
        //creating singleton instance of Wifi Scan manager and setting call back for it.
        WifiScanManager.getInstance(this).setListener(this);
        locationButton.setEnabled(true);
        geoFenceButton.setEnabled(true);
        callLogButton.setEnabled(true);
        BLEButton.setEnabled(true);
        BLESpecificButton.setEnabled(true);
    }

    @Override
    public void permissionDenied() {
        super.permissionDenied();
        locationButton.setEnabled(false);
        geoFenceButton.setEnabled(false);
        callLogButton.setEnabled(false);
        BLEButton.setEnabled(false);
        BLESpecificButton.setEnabled(false);
    }
}
