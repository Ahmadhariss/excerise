package aiia.app.developer.sample.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import aiia.app.developer.sample.services.ManageGeofences;

/**
 * Created by Mad on 4/13/2017.
 */

public class AutoStart extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent addGeoFenceIntent = new Intent(context, ManageGeofences.class);
        context.startService(addGeoFenceIntent);
    }
}
